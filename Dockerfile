FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > okular.log'

COPY okular .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' okular
RUN bash ./docker.sh

RUN rm --force --recursive okular
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD okular
